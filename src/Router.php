<?php

namespace Mvc4u;

class Router
{
	protected $application;

	protected $controllerName;

	protected $controllerClass;

	protected $actionName;

	public function setApplication($application)
	{
		$this->application = $application;
	}

	public function route()
	{
		if($this->isCLI())
		{
			$argv = $_SERVER['argv'];

			array_shift($argv);

			$this->getControllerFromUrlParts($argv);
			$this->getActionFromUrlParts($argv);
		}
		else {
			$requestUri = $this->getRequestUri();

			$urlParts = explode('/', $requestUri);

			array_shift($urlParts);

			$this->getControllerFromUrlParts($urlParts);
			$this->getActionFromUrlParts($urlParts);
		}

		$this->controllerClass = '\App\Controller\\'.$this->controllerName;

		if(!class_exists($this->controllerClass))
		{
			$this->show404();
		}
		else {
			return true;
		}
	}

	public function dispatch()
	{
		$controller = new $this->controllerClass();
		$controller->setApplication($this->application);

		$methodVariable = array($controller, $this->actionName);

		if(!is_callable($methodVariable))
		{
			$this->show404();
		}
		else {
			$controller->dispatch($this->actionName);
		}
	}

	protected function show404()
	{
		http_response_code(404);
		echo '404 Page not found';
		exit();
	}

	private function isCLI()
	{
		return (php_sapi_name() == 'cli');
	}

	private function getControllerFromUrlParts($urlParts)
	{
		$this->controllerName = $this->getClassName((!empty($urlParts[0]))? $urlParts[0] : 'index');
	}

	private function getActionFromUrlParts($urlParts)
	{
		$this->actionName = $this->getMethodName((!empty($urlParts[1]))? $urlParts[1] : 'index');
	}

	private function getClassName($name)
	{
		$parts = explode('-', $name);

		return str_replace(' ', '', ucwords(strtolower(join(' ', $parts))));
	}

	private function getMethodName($name)
	{
		$parts = explode('-', $name);

		$parts[] = 'action';

		return str_replace(' ', '', lcfirst(ucwords(strtolower(join(' ', $parts)))));
	}

	private function getRequestUri()
	{
		$requestUri = $_SERVER['REQUEST_URI'];
		if(preg_match('/^([^ \?]+)/', $requestUri, $matches))
		{
			$requestUri = $matches[1];
		}
		return $requestUri;
	}
}