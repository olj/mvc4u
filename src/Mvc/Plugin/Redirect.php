<?php

namespace Mvc4u\Mvc\Plugin;

class Redirect
{
	/**
	 * @param string $url
	 * @param bool $exit
	 * @param int $code
	 */
	public function toUrl($url, $exit = true, $code = 302)
	{
		header('Location: ' . $url, true, $code);

		if($exit)
		{
			exit();
		}
	}
}