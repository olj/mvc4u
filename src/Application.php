<?php

namespace Mvc4u;

use \Mvc4u\Library\ArrayUtils;
use \Mvc4u\Db\Adapter as DbAdapter;

class Application
{
	protected $config;

	/**
	 * @var Router;
	 */
	protected $router;

	/**
	 * @var DbAdapter[]
	 */
	protected $dbAdapters = array();

	public static function init($config = null)
	{
		$application = new self();

		$localConfigPath = 'config/local.config.php';
		if(file_exists($localConfigPath))
		{
			$config = ArrayUtils::merge($config, require $localConfigPath);
		}

		$application->config = $config;
		$application->initDbAdapters();
		$application->router = new Router();

		return $application;
	}

	public function run()
	{
		$this->router->route();
		$this->router->setApplication($this);

		$this->dispatch();
	}

	protected function dispatch()
	{
		$this->router->dispatch();
	}

	protected function initDbAdapters()
	{
		$config = $this->config;

		if(!empty($config['db']) && !empty($config['db']['adapters']))
		{
			foreach ($config['db']['adapters'] as $key => $adapterConfig)
			{
				try {
					$this->dbAdapters[$key] = new DbAdapter($adapterConfig);
				}
				catch (\Exception $exception)
				{
					$message = sprintf('Unable to connect to %s database', $key);
					throw new \Exception($message);
				}
			}
		}
	}

	public function getConfig()
	{
		return $this->config;
	}

	/**
	 * @param $name
	 * @return mixed
	 * @throws \Exception
	 */
	public function get($name)
	{
		if(!class_exists($name))
		{
			throw new \Exception('Unable to load class');
		}
		$instance = new $name();

		if($instance instanceof Application\AwareInterface)
		{
			$instance->setApplication($this);
		}

		return $instance;
	}

	/**
	 * @param string $adapterName
	 * @return Db\Adapter|null
	 */
	public function getDbAdapter($adapterName)
	{
		return (isset($this->dbAdapters[$adapterName]))? $this->dbAdapters[$adapterName] : null;
	}
}