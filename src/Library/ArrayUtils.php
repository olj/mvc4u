<?php

namespace Mvc4u\Library;

class ArrayUtils
{
	/**
	 * Merge two arrays together.
	 *
	 * If an integer key exists in both arrays and preserveNumericKeys is false, the value
	 * from the second array will be appended to the first array. If both values are arrays, they
	 * are merged together, else the value of the second array overwrites the one of the first array.
	 *
	 * @param  array $a
	 * @param  array $b
	 * @param  bool  $preserveNumericKeys
	 * @return array
	 */
	public static function merge(array $a, array $b, $preserveNumericKeys = false)
	{
		foreach ($b as $key => $value) {
			if (array_key_exists($key, $a)) {
				if (is_int($key) && !$preserveNumericKeys) {
					$a[] = $value;
				} elseif (is_array($value) && is_array($a[$key])) {
					$a[$key] = static::merge($a[$key], $value, $preserveNumericKeys);
				} else {
					$a[$key] = $value;
				}
			} else {
				$a[$key] = $value;
			}
		}

		return $a;
	}
}