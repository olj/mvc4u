<?php

namespace Mvc4u;

/**
 * Class Controller
 * @package Application
 * @method \Mvc4u\Mvc\Plugin\Redirect redirect()
 */
class Controller
{
	/**
	 * @var \Mvc4u\Application
	 */
	protected $application;

	/**
	 * @param \Mvc4u\Application $application
	 */
	public function setApplication(Application $application)
	{
		$this->application = $application;
	}

	/**
	 * @return \Mvc4u\Application
	 */
	public function getApplication()
	{
		return $this->application;
	}

	protected function jsonResponse($data)
	{
		if($this->isCLI())
		{
			$this->output($data);
		}
		else {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($data);
		}
	}

	protected function output($output, $replace = false)
	{
		if(is_array($output))
		{
			$output = print_r($output, true);
		}

		if(!$replace)
		{
			echo PHP_EOL . $output;
		}
		else {
			echo "\r" . $output;
		}
	}

	public function dispatch($actionName)
	{
		return $this->$actionName();
	}

	public function __call($method, $params)
	{
		$plugin = $this->getPlugin($method, $params);
		if (is_callable($plugin)) {
			return call_user_func_array($plugin, $params);
		}
		return $plugin;
	}

	protected function getPlugin($name, $params)
	{
		$name = ucfirst(strtolower($name));
		$pluginName = '\Mvc4u\Mvc\Plugin\\'.$name;

		return new $pluginName($params);
	}

	protected function isCLI()
	{
		return (php_sapi_name() == 'cli');
	}
}