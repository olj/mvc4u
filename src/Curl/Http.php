<?php

namespace Mvc4u\Curl;

class Http
{
	protected $client;

	protected $url;

	public function __construct($url = null)
	{
		$this->client = curl_init();

		if($url)
		{
			$this->setUrl($url);
		}

		curl_setopt_array($this->client, array(
			CURLOPT_RETURNTRANSFER => 1,
		));
	}

	/**
	 * @param string $url
	 */
	public function setUrl($url)
	{
		$this->url = $url;
	}

	/**
	 * @param null|array $params
	 * @return mixed
	 */
	public function post($params = null)
	{
		curl_setopt_array($this->client, array(
			CURLOPT_URL => $this->url,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $params
		));
		$response = curl_exec($this->client);

		return $response;
	}

	/**
	 * @param null|array $params
	 * @return mixed
	 */
	public function get($params = null)
	{
		curl_setopt_array($this->client, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $this->url . $this->getQueryString($params),
			CURLOPT_POST => 0,
			CURLOPT_POSTFIELDS => null
		));
		$response = curl_exec($this->client);

		return $response;
	}

	/**
	 * @param array $headers
	 */
	public function setHttpHeaders($headers)
	{
		curl_setopt($this->client, CURLOPT_HTTPHEADER, $headers);
	}

	public function close()
	{
		curl_close($this->client);
	}

	/**
	 * @param array $params
	 * @return string
	 */
	private function getQueryString($params)
	{
		if(is_array($params))
		{
			return '?' . http_build_query($params);
		}
		return '';
	}
}