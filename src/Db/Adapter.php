<?php

namespace Mvc4u\Db;

class Adapter
{
	/**
	 * @var \PDO
	 */
	protected $adapter;

	/**
	 * Adapter constructor.
	 * @param array $config
	 */
	public function __construct($config)
	{
		$dns = $config['driver'] . ':host=' . $config['host'] .
			((!empty($config['port'])) ? (';port=' . $config['port']) : '') .
			';dbname=' . $config['dbname'] . ';charset=' .
			((!empty($config['charset']))? $config['charset'] : 'utf8')
		;

		$adapter = new \PDO($dns, $config['username'], $config['password']);
		$adapter->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
		$adapter->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		$this->setAdapter($adapter);
	}

	/**
	 * @return \PDO
	 */
	public function getAdapter()
	{
		return $this->adapter;
	}

	/**
	 * @param \PDO $adapter
	 */
	public function setAdapter($adapter)
	{
		$this->adapter = $adapter;
	}
}