<?php

namespace Mvc4u\Db;

use Mvc4u\Application;
use Mvc4u\Application\AwareInterface;

class Table implements AwareInterface
{
	/**
	 * @var Application
	 */
	protected $application;

	protected $adapterName;

	public function setApplication(Application $application)
	{
		$this->application = $application;
	}

	public function getApplication()
	{
		return $this->application;
	}

	protected function getAdapter($adapterName = null)
	{
		if(!$adapterName && $this->adapterName)
		{
			$adapterName = $this->adapterName;
		}

		$config = $this->getApplication()->getConfig();

		if(!$adapterName && !empty($config['db']['defaultAdapter']))
		{
			$adapterName = $config['db']['defaultAdapter'];
		}

		if($this->application->getDbAdapter($adapterName))
		{
			return $this->application->getDbAdapter($adapterName)->getAdapter();
		}
		else {
			throw new \Exception('Unable to find db adapter');
		}
	}
}