<?php

namespace Mvc4u\Application;

use Mvc4u\Application as Application;

interface AwareInterface
{
	/**
	 * Set service manager
	 *
	 * @param Application $application
	 */
	public function setApplication(Application $application);
}